# sphinx-js-docs

sphinx-js でドキュメントを生成するサンプルコードです。以下のコマンドを実行すると `docs/_build/html/index.html` が生成されます。

```
pip3 install -r requirements.txt
npm install
npm run build
```
