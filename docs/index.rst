.. sphinx-js-docs documentation master file, created by
   sphinx-quickstart on Wed Apr 14 01:57:56 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to sphinx-js-docs's documentation!
==========================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

.. js:autofunction:: main

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
